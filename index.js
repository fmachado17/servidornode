const express = require ('express');
const conectarDB = require('./config/db');
const cors= require('cors');

const app = express();

conectarDB();

app.use(cors());

app.use(express.json({extended:true}));

// puerto de la app
const port = process.env.PORT || 5000;

app.use('/api/usuarios', require('./routes/usuarios'))
app.use('/api/auth', require('./routes/auth'))
app.use('/api/proyectos', require('./routes/proyectos'))


    
app.listen(port,'0.0.0.0',()=>
    console.log(`El servidor esta funcionando en el puerto ${port}`)
);